import React from 'react';

import BlockContent from '@sanity/block-content-to-react';

import { fluidImageNotNull } from '../utils/helper';
import Fade from 'react-reveal/Fade';

import Img from 'gatsby-image';

import { PortfolioPageDataQuery } from '../../graphqlTypes';

interface Props {
  referrer: string;
  portfolio: PortfolioPageDataQuery['sanityPortfolio'];
}

function About(props: Props) {
  const { portfolio } = props;
  return (
    <Fade cascade>
      <div>
        <section className="about">
          <BlockContent blocks={portfolio!._rawAbout} />
        </section>
      </div>
      <div>
        <section className="overall-experience">
          <h2>Experience</h2>
          <ul>
            {/* <>
              <span className="circle circle-first-child">?</span>
              <li
                style={{ opacity: 0.5 }}
              >{`Lead Product Designer, ${referrer}?`}</li>
            </> */}
            {portfolio!.experience?.map((experience, key) => {
              return (
                <span key={key}>
                  <span className="circle">
                    <Img
                      fluid={fluidImageNotNull(
                        experience?.companyLogo?.asset?.fluid!
                      )}
                    ></Img>
                  </span>
                  <li key={key}>
                    {experience?.designation}, {experience?.company}
                  </li>
                </span>
              );
            })}
          </ul>
        </section>
      </div>
    </Fade>
  );
}

export default About;
