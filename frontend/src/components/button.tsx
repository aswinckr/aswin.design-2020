import styled from 'styled-components';
import { backgroundColor, primaryColor, textColor } from '../components/colors';

// Todo - Button width is uneven on smaller screens

interface ButtonProps {
  white?: boolean;
}

const Button = styled.button<ButtonProps>`
  border-radius: 6px;
  border: 1px solid ${primaryColor};
  background-color: transparent;
  color: ${primaryColor};
  padding: 0.5rem 1rem;
  transition: 0.2s;
  min-width: 115px;
  &:hover {
    color: ${props => (props.white ? primaryColor : '#fff')};
    background: ${props => (props.white ? '#fff' : primaryColor)};
    cursor: pointer;
  }
  &:focus {
    outline: none;
  }
`;

export default Button;
