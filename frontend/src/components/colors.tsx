import theme from 'styled-theming';
import { LightenDarkenColor } from '../utils';

// Basic Colors

export const primaryColor: string = '#F35242';
export const primaryHoverColor: string = LightenDarkenColor(primaryColor, -30);
export const darkBackgroundColor: string = '#1c1d25';
export const lightBackgroundColor: string = '#fafafd';
export const linkColor: string = primaryColor;
export const headerLinkColor: string = '#FFF';
export const additionalThumbnailColor: string = '#EAF1FF';
export const highlightColor: string = '#ffc600';
// export const testimonialBg: string = '#0e1015';

// Toggle Colors

export const textColor = theme('mode', {
  dark: 'rgba(255,255,255)',
  light: '#24292e',
});

export const backgroundColor = theme('mode', {
  dark: darkBackgroundColor,
  light: lightBackgroundColor,
});

export const themeBackgroundColor = theme('mode', {
  dark: '#1c1d25',
  light: '#FFF',
});
export const contrastBackgroundColor = theme('mode', {
  light: '#1c1d25',
  dark: '#F5f5f5',
});

export const headerBackgroundColor = theme('mode', {
  dark: '#111217',
  light: '#111217',
});

export const interfaceIconsColor = theme('mode', {
  dark: '#FFFFFF',
  light: '#262A2B',
});

export const sidebarInactiveColor = theme('mode', {
  dark: '#a5a5a5',
  light: '#9a9caf',
});

export const borderLineColor = theme('mode', {
  dark: '#525252',
  light: '#e2e2e2',
});

export const testimonialBg = theme('mode', {
  dark: '#0e1015',
  light: '#fff',
});

export const portfolioThumbnailBg = theme('mode', {
  dark: '#242631',
  light: '#fff',
});
