import React from 'react';
// import { useStaticQuery, graphql } from 'gatsby';
// import Img from 'gatsby-image';

// /*
//  * This component is built using `gatsby-image` to automatically serve optimized
//  * images with lazy loading and reduced file sizes. The image is loaded using a
//  * `useStaticQuery`, which allows us to load the image from directly within this
//  * component, rather than having to pass the image data down from pages.
//  *
//  * For more information, see the docs:
//  * - `gatsby-image`: https://gatsby.dev/gatsby-image
//  * - `useStaticQuery`: https://www.gatsbyjs.org/docs/use-static-query/
//  */

// interface CompanyLogoProps {
//   logo: string | undefined;
// }

// const CompanyLogo = ({ logo }: CompanyLogoProps) => {
//   switch (logo) {
//     case 'grab.png': {
//       const data = useStaticQuery(graphql`
//         query {
//           placeholderImage: file(relativePath: { eq: "logo.png" }) {
//             childImageSharp {
//               fluid(maxWidth: 300) {
//                 ...GatsbyImageSharpFluid
//               }
//             }
//           }
//         }
//       `);

//       return <Img fluid={data.placeholderImage.childImageSharp.fluid} />;
//     }

//     case 'housing.png': {
//       const data = useStaticQuery(graphql`
//         query {
//           placeholderImage: file(relativePath: { eq: "housing.png" }) {
//             childImageSharp {
//               fluid(maxWidth: 300) {
//                 ...GatsbyImageSharpFluid
//               }
//             }
//           }
//         }
//       `);

//       return <Img fluid={data.placeholderImage.childImageSharp.fluid} />;
//     }
//     case 'zeta.png': {
//       const data = useStaticQuery(graphql`
//         query {
//           placeholderImage: file(relativePath: { eq: "zeta.png" }) {
//             childImageSharp {
//               fluid(maxWidth: 300) {
//                 ...GatsbyImageSharpFluid
//               }
//             }
//           }
//         }
//       `);

//       return <Img fluid={data.placeholderImage.childImageSharp.fluid} />;
//     }
//     case 'naukri.png': {
//       const data = useStaticQuery(graphql`
//         query {
//           placeholderImage: file(relativePath: { eq: "naukri.png" }) {
//             childImageSharp {
//               fluid(maxWidth: 300) {
//                 ...GatsbyImageSharpFluid
//               }
//             }
//           }
//         }
//       `);

//       return <Img fluid={data.placeholderImage.childImageSharp.fluid} />;
//     }
//     case 'default':
//       return (
//         <img src="https://placehold.it/200x200" alt="company-placeholder" />
//       );
//   }
//   return <img src="https://placehold.it/200x200" alt="company-placeholder" />;
// };

// export default CompanyLogo;

// '

const uselessStuff = () => {
  return <></>;
};

export default uselessStuff;
