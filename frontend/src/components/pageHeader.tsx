import React from 'react';
import Fade from 'react-reveal/Fade';
import Img from 'gatsby-image';
import { fixedImageNotNull } from '../utils/helper';
import { FixedObject } from 'gatsby-image';
import styled from 'styled-components';
import { device } from '../utils';
import { primaryColor } from './colors';
// import ThemedLayout from '../components/theme';

interface PageHeaderProps {
  sanityImage: FixedObject;
  title?: string;
}

const PageHeaderStyles = styled.div`
  width: 100%;
  .portfoilo-header {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap-reverse;
    padding: 60px 0;
    align-items: center;

      @media ${device.tablet} {
        margin-left: auto;
      }

      position: relative;
      top: -20px;
    }
    h1 {
      display: block;
      flex: 1;
    }

    img {
      border-radius: 100px;
      border: 3px solid ${primaryColor};
      @media ${device.tablet} {
        margin-left: auto;
        top: -35px;
      }
      top: -5px;
      position: relative;
    }
  }
`;

const PageHeader = (props: PageHeaderProps) => (
  <PageHeaderStyles>
    <Fade>
      <div className="portfoilo-header">
        <h1>{props.title ? props.title : `Hey! I'm Aswin.`}</h1>

        <Img
          fixed={fixedImageNotNull(props.sanityImage)}
          className="portfolio-image"
        ></Img>
      </div>
    </Fade>
  </PageHeaderStyles>
);

export default PageHeader;
