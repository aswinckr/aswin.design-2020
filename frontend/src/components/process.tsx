import React from 'react';
import styled from 'styled-components';
import { textColor, backgroundColor, testimonialBg } from './colors';

interface ProcessProps {
  processList?: [string];
  timeDuration?: string;
}

const ProcessStyles = styled.div`
  padding: 24px;
  background: ${testimonialBg};
  border-radius: 6px;
  h4:first-child {
    border-bottom: 1px solid ${textColor};
    padding-bottom: 12px;
  }
  .process-list {
    display: flex;
    div:first-child {
      width: 30px;
      height: 30px;
      flex-shrink: 0;
      border-radius: 100px;
      text-align: center;
      font-weight: 600;
      background: ${backgroundColor};
    }
    p {
      padding: 0 0 0 16px;
      font-weight: 600;
    }
  }
`;

const Process = (props: ProcessProps) => {
  const {
    processList = ['this is the first process', 'this is the second process'],
    timeDuration = '3 months',
  } = props;
  return (
    <ProcessStyles>
      {/* <h4>Total Time Duration: {timeDuration}</h4> */}
      <p style={{ opacity: 0.5, fontSize: '0.9em' }}>
        DISCLAIMER: The design process explained below was adopted to meet the
        timelines and specific objectives of the project. If you'd like to
        understand the process I would recommend for your project, It would be
        my pleasure to brainstorm over a call.
      </p>
      {processList.map((process, key) => {
        return (
          <div key={key} className="process-list">
            <div>{key + 1}</div>
            <p>{process}</p>
          </div>
        );
      })}
    </ProcessStyles>
  );
};

export default Process;
