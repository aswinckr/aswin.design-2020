import React from 'react';
import { Link } from '@reach/router';
import BlockContent from '@sanity/block-content-to-react';
import Button from '../components/button';

import Fade from 'react-reveal/Fade';
import { WorkDisplayImg } from '../styles/portfolio';

// import CompanyLogo from '../components/companyLogos';
import { PortfolioPageDataQuery, SanityWorkDetails } from '../../graphqlTypes';

interface Props {
  allWork: [SanityWorkDetails];
  work: SanityWorkDetails;
  refIndex: number;
  searchParam: string;
  portfolio: PortfolioPageDataQuery['sanityPortfolio'];
}

function Projects(props: Props) {
  const { work, refIndex: index, portfolio, searchParam } = props;
  return (
    <div key={index}>
      <section className={`work-section work-section-${work?.companyName}`}>
        <Fade>
          {work?.companyName !== 'Side Projects' ? (
            <h2>Work at {work?.companyName}</h2>
          ) : (
            <h2>Side Projects</h2>
          )}
          {/* <div className="key-achievements">
            <h4>Key Achievements</h4>
            <BlockContent blocks={allWork[index].keyAchievements} />
          </div> */}
        </Fade>

        {work.work
          ?.filter(
            featuredWork =>
              featuredWork?.featured === true && featuredWork?.featured !== null
          )
          .map((workThumbnail, indexThumbnail) => {
            if (workThumbnail && workThumbnail === null) return;
            else
              return (
                <Fade key={indexThumbnail}>
                  <Link to={`${workThumbnail?.slug?.current!}/${searchParam}`}>
                    <div
                      className="portfolio-showcase-wrapper"
                      // style={{
                      //   background: `${
                      //     workThumbnail?.themeColor
                      //       ? workThumbnail?.themeColor?.hex
                      //       : '#f5f5f5'
                      //   }`,
                      // }}
                    >
                      <h2>{workThumbnail?.title}</h2>

                      <div className="portfolio-preview-problem">
                        <BlockContent
                          blocks={workThumbnail?.problemStatement}
                        />
                        <Button white>Learn More</Button>
                      </div>
                      {/* <div className="portfolio-preview-solution">
                        <h4>Solution & Impact</h4>
                        <BlockContent blocks={workThumbnail?.solution} />
                      </div> */}

                      {workThumbnail?.mainImage && (
                        <WorkDisplayImg
                          className="portfolio-preview-image grow"
                          src={
                            portfolio!.work![index]?.work![indexThumbnail]
                              ?.mainImage?.asset?.url!
                          }
                        />
                      )}
                    </div>
                  </Link>
                </Fade>
              );
          })}

        {/* Ongoing projects  */}
        <div className="other-ongoing-projects">
          {work.work?.filter(
            featuredWork =>
              featuredWork?.featured !== null &&
              featuredWork?.featured === false
          ) &&
            work.work?.filter(
              featuredWork =>
                featuredWork?.featured !== null &&
                featuredWork?.featured === false
            ).length > 0 && <h4>Other projects</h4>}
          <Fade>
            <div className="additional-work-thumbnails">
              {work.work
                ?.filter(
                  featuredWork =>
                    featuredWork?.featured === false &&
                    featuredWork?.featured !== null
                )
                .map((workThumbnail, indexThumbnail) => {
                  if (workThumbnail && workThumbnail === null) return;
                  else
                    return (
                      <Link
                        to={`${workThumbnail?.slug?.current!}/${searchParam}`}
                        key={indexThumbnail}
                      >
                        <div
                          className="additional-work-thumbnail soft-shadow grow"
                          style={{
                            background: `${
                              workThumbnail?.themeColor
                                ? workThumbnail?.themeColor?.hex
                                : '#f5f5f5'
                            }`,
                          }}
                        >
                          <h3>{workThumbnail?.title}</h3>
                        </div>
                      </Link>
                    );
                })}
            </div>
          </Fade>
        </div>
      </section>
    </div>
  );
}

export default Projects;
