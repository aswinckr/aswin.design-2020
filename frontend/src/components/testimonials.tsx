import React from 'react';
import { Element } from 'react-scroll';
import { SanityPortfolio, SanityTestimonial } from '../../graphqlTypes';
import Fade from 'react-reveal/Fade';
import BlockContent from '@sanity/block-content-to-react';

// interface TestimonialProps {
//   allTestimonials: [SanityTestimonial];
//   portfolio: SanityPortfolio;
//   children: ReactChildren;
// }

type Props = {
  allTestimonials: [SanityTestimonial];
  portfolio: SanityPortfolio;
};

const Testimonials = (props: Props) => {
  const { allTestimonials, portfolio } = props;
  return (
    <Element name="whatPeopleSay">
      <section className="what-people-say">
        <h2>Testimonials</h2>
        {allTestimonials.map((testimonial, index) => (
          <div key={index}>
            <Fade>
              <div className="what-people-say-testimonial soft-shadow">
                <div className="author-photo">
                  <img
                    src={
                      portfolio.testimonials !== null
                        ? portfolio.testimonials![index]?.authorPhoto?.asset
                            ?.fluid?.src!
                        : 'https://placehold.it/50x50'
                    }
                    alt={testimonial.author!}
                    width="100"
                    height="100"
                  />
                </div>
                <span key={index}>
                  <BlockContent blocks={testimonial.statement} />
                  <p className="what-people-say-author">{testimonial.author}</p>
                </span>
              </div>
            </Fade>
          </div>
        ))}
      </section>
    </Element>
  );
};

export default Testimonials;
