import React from 'react';

function TestOne() {
  console.log('test1 is executed');
  return (
    <div>
      <h1>This is test one</h1>
    </div>
  );
}

export default TestOne;
