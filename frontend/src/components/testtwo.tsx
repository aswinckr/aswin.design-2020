import React from 'react';

function TestTwo() {
  console.log('test2 is executed');
  return (
    <div>
      <h1>This is test two</h1>
    </div>
  );
}

export default TestTwo;
