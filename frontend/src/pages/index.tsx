import React from 'react';
import Layout from '../components/layout';
import { graphql } from 'gatsby';
import BlockContent from '@sanity/block-content-to-react';
import { HomePageDataQuery } from '../../graphqlTypes';
import { RouteComponentProps, Link } from '@reach/router';
import styled from 'styled-components';
import Button from '../components/button';
import PageHeader from '../components/pageHeader';
import { fixedImageNotNull } from '../utils/helper';

interface Props extends RouteComponentProps {
  data: HomePageDataQuery;
}

const HomePageStyles = styled.div`
  padding: 40px;
`;

const index = (props: Props) => {
  const { location } = props;
  const {
    data: { sanityPortfolio: portfolio },
  } = props;
  return (
    <Layout params={location && location.search}>
      <HomePageStyles>
        <PageHeader
          sanityImage={fixedImageNotNull(
            portfolio!.profilePhoto?.asset?.fixed!
          )}
        ></PageHeader>

        <BlockContent blocks={portfolio?._rawAbout} />
        <div className="all-projects" style={{ marginBottom: 80 }}>
          <Link to={`portfolio/${props.location ? props.location.search : ''}`}>
            <Button> See my Portfolio ( Invitation only )</Button>
          </Link>
        </div>
      </HomePageStyles>
    </Layout>
  );
};

export const query = graphql`
  query HomePageData {
    sanityPortfolio(slug: { current: { eq: "portfolio" } }) {
      _rawAbout
      profilePhoto {
        asset {
          fixed(width: 125, height: 125) {
            ...GatsbySanityImageFixed
          }
        }
      }
    }
  }
`;

export default index;
