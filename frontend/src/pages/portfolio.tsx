import React, { useState, CSSProperties } from 'react';
import { RouteComponentProps } from '@reach/router';
import { graphql } from 'gatsby';

import BlockContent from '@sanity/block-content-to-react';

import { fixedImageNotNull } from '../utils/helper';
import Fade from 'react-reveal/Fade';
import { IndexPageStyles } from '../styles/portfolio';
import PageHeader from '../components/pageHeader';

// import CompanyLogo from '../components/companyLogos';
import Layout from '../components/layout';
import Projects from '../components/projects';
import About from '../components/about';
import {
  PortfolioPageDataQuery,
  SanityWorkDetails,
  SanityTestimonial,
} from '../../graphqlTypes';

interface Props extends RouteComponentProps {
  data: PortfolioPageDataQuery;
}

// const getPosition = () => ({
//   x: window.pageXOffset,
//   y: window.pageYOffset,
// });

const Index = (props: Props) => {
  const {
    data: { sanityPortfolio: portfolio },
    location,
  } = props;

  const allWork: [SanityWorkDetails] = portfolio!._rawWork;
  const allTestimonials: [SanityTestimonial] = portfolio!._rawTestimonials;

  const referrer = location ? location.search.split('=')[1] : '';
  const searchParam = location ? location.search : '';
  const accessGrantKeywords = portfolio!.accessGrant;

  // useEffect(() => {
  //   const scrollPosition = useWindowScrollPosition();
  //   if (scrollPosition.y > scrollPositionForSideNav) {
  //     setSideNavStyle({ position: 'sticky' });
  //   }
  // }, [sideNavStyle]);

  console.log(referrer);

  if (accessGrantKeywords?.includes(referrer)) {
    return (
      <Layout params={searchParam}>
        <IndexPageStyles>
          {/* Header Section */}
          <PageHeader
            title={`Aswin's Portfolio`}
            sanityImage={fixedImageNotNull(
              portfolio!.profilePhoto?.asset?.fixed!
            )}
          />

          {/* Sidebar */}

          {/* About Me */}

          <div className="portfolio-preview">
            <About referrer={referrer} portfolio={portfolio} />

            {/* Projects */}
            {allWork.map((work, index) => (
              <Projects
                allWork={allWork}
                work={work}
                refIndex={index}
                key={index}
                searchParam={searchParam}
                portfolio={portfolio}
              />
            ))}

            {/* Testimonials */}
            <div>
              <section className="what-people-say">
                <h2>Testimonials</h2>
                {allTestimonials.map((testimonial, index) => (
                  <div key={index}>
                    <Fade>
                      <div className="what-people-say-testimonial soft-shadow">
                        <div className="author-photo">
                          <img
                            src={
                              portfolio!.testimonials !== null
                                ? portfolio!.testimonials[index]?.authorPhoto
                                    ?.asset?.fluid?.src!
                                : 'https://placehold.it/50x50'
                            }
                            alt={testimonial.author!}
                            width="100"
                            height="100"
                          />
                        </div>
                        <span key={index}>
                          <BlockContent blocks={testimonial.statement} />
                          <p className="what-people-say-author">
                            {testimonial.author}
                          </p>
                        </span>
                      </div>
                    </Fade>
                  </div>
                ))}
              </section>
            </div>

            {/* Culture Fit */}
            <div>
              <Fade>
                <section className="what-am-i-like-to-work-with">
                  <h2>More about me</h2>
                  <BlockContent blocks={portfolio!._rawMyPreferences} />
                </section>
              </Fade>
            </div>
            <div>
              <Fade>
                <section className="so-are-we-moving-forward">
                  <h2>Contact</h2>
                  <p>
                    If you liked what you saw, write to me at{' '}
                    <a
                      href="https://mail.google.com/mail/u/0/#inbox?compose=new"
                      target="_blank"
                    >
                      me@aswin.design.
                    </a>
                  </p>
                </section>
              </Fade>
            </div>
          </div>
        </IndexPageStyles>
      </Layout>
    );
  } else if (!referrer) {
    return (
      <>
        <Layout>
          <h1>Hello, Visitor!</h1>
          <p>
            You're here because you stumbled on my profile or one of my
            navigation brought you here
          </p>
          <p>
            This page at the moment is only visible to invited people. If you've
            received an invitation,{' '}
            <strong>
              please follow the same link sent to you. The link must have a
              trailing "query string". It looks something like "?xyz=abc".{' '}
            </strong>
            You need that to view my portfolio. Sorry for the inconvenience
          </p>
          <p>
            As for the others - I'll open up my page soon for public. Watch this
            space :)
          </p>
        </Layout>
      </>
    );
  } else
    return (
      <>
        <Layout>
          <h1>Please wait.. Checking your invitation</h1>
          <p>
            You need an invitation to view this page. If you have been invited
            already, please visit the URL sent to your official email.
          </p>
        </Layout>
      </>
    );
};

export const query = graphql`
  query PortfolioPageData {
    sanityPortfolio(slug: { current: { eq: "portfolio" } }) {
      id
      _rawAbout

      profilePhoto {
        asset {
          fixed(width: 125, height: 125) {
            ...GatsbySanityImageFixed
          }
        }
      }
      profilePhoto {
        asset {
          fluid {
            srcSet
          }
        }
      }
      experience {
        company
        companyLogo {
          asset {
            fluid {
              src
            }
          }
        }
        designation
        currentlyWorking
      }
      testimonials {
        authorPhoto {
          asset {
            fluid {
              src
            }
          }
        }
      }
      _rawWork(resolveReferences: { maxDepth: 10 })
      work {
        companyName

        work {
          featured
          themeColor {
            hex
          }
          title
          _rawProblemStatement
          _rawSolution
          mainImage {
            asset {
              url
            }
          }
        }
      }
      _rawTestimonials
      _rawMyPreferences
      accessGrant
    }
  }
`;

export default Index;
