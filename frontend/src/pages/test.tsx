import React from 'react';
import { Link, graphql } from 'gatsby';
import { AnotherGreatQueryNameQuery } from '../../graphqlTypes';
import Process from '../components/process';

import Layout from '../components/layout';

import SEO from '../components/seo';
import Img, { FixedObject } from 'gatsby-image';

interface Props {
  data: AnotherGreatQueryNameQuery;
}

const Test = ({ data }: Props) => {
  // @ts-ignore
  const gatsbyImage: FixedObject = {
    ...data.sanityPortfolio?.profilePhoto?.asset?.fixed,
  };

  console.log(gatsbyImage);
  return (
    <Layout>
      {/* {console.log(data)} */}
      <SEO title="Test" keywords={[`Test`]} />
      {/* {console.log(data)} */}
      <h1>Welcome to Test page</h1>
      <Process></Process>
      {/* {checkProperties()} */}
      {/* <Img fixed={data.sanityPortfolio?.profilePhoto?.asset?.fixed!} /> */}
      <Link to="/">Go to home</Link>
    </Layout>
  );
};

export const query = graphql`
  query AnotherGreatQueryName {
    sanityPortfolio(slug: { current: { eq: "portfolio" } }) {
      profilePhoto {
        asset {
          fixed(width: 150, height: 150) {
            ...GatsbySanityImageFixed
          }
        }
      }
    }
  }
`;

export default Test;
