import styled from 'styled-components';
import {
  linkColor,
  sidebarInactiveColor,
  backgroundColor,
  portfolioThumbnailBg,
  borderLineColor,
  primaryColor,
  testimonialBg,
  textColor,
} from '../components/colors';
import { device } from '../utils';

export const IndexPageStyles = styled.div`
  display: flex;
  flex-wrap: wrap;


  section {
    padding-bottom: 60px;
  }
  .overall-experience {
    ul {
      margin-left: 14px;
      border-left: 1px solid ${borderLineColor};
      position: relative;
      .circle {
        width: 30px;
        height: 30px;
        border-radius: 100px;
        background: ${sidebarInactiveColor};
        display: block;
        position: absolute;
        left: -15px;
        overflow: hidden;
        border: 4px solid ${backgroundColor};
        div:nth-child(1) {
          width: 22px;
          height: 22px;
        }
      }

      .circle-first-child {
        background: ${backgroundColor}!important;
        border: 1px dashed ${sidebarInactiveColor};
        padding-left: 10px;
        color: #7d7d7d;
      }
      li {
        list-style: none;
        padding-left: 32px;
        margin-bottom: 32px;
        font-weight: 600;
      }
    }
  }

  .portfolio-sidebar {
    margin-left: 0;
    display: none;
    @media ${device.tablet} {
      display: block;
    }

    li {
      list-style: none;
      padding-bottom: 16px;
    }

    a {
      color: ${sidebarInactiveColor};
      cursor: pointer;
    }
    .active {
      color: ${linkColor}!important;
      font-weight: 700;
      /* padding-left: 16px; */
      /* .nav-highlighter {
        opacity: 100;
      } */
    }
    .nav-highlighter {
      display: block;
      position: absolute;
      left: 0;
      width: 4px;
      height: 30px;
      background: ${linkColor};
      opacity: 0;
    }
  }
  .portfolio-summary {
    /* flex: 1; */
    width: 25%;
    position: relative;
  }
  .key-achievements {
    padding-top: 4px;
    border-radius: 4px;
    h4 {
      margin: 18px 12px 32px 0;
    }
    ul {
      margin-left: 16px;
      li {
        list-style: circle;
      }
    }
  }
  .work-section {
    margin-bottom: 60px;
  }
  .portfolio-preview {
    /* flex: 2; */
    margin: 0 auto;
    @media ${device.tablet} {
      width: 75%;
    }
    width: 100%;
  }
  .portfolio-preview-image {
    width: 100%;
    overflow: hidden;
    min-height: 300px;
    display: flex;
  }
  .portfolio-showcase-wrapper {
    margin: 50px 0 50px 0;
    overflow: hidden;
    border-radius: 4px;
    display: flex;
    flex-wrap: wrap;
    /* background: ${portfolioThumbnailBg}; */
    /* background: #000; */
    color: ${textColor};
    font-size: 0.95em;
    border: 1px solid ${borderLineColor};
    h2 {
      color: ${textColor};
      padding: 8px 24px 0 24px;
      width: 100%;
      margin: 24px 0 8px 0;
      position: relative;
    }
    .slowRepetitiveMotion {
      position: absolute;
      padding-left: 8px;
    }
    .portfolio-preview-problem {
      padding: 0 24px 60px 24px;
    }
    .portfolio-preview-solution {
      padding: 0 24px 0 0;
    }
    .portfolio-preview-problem,
    .portfolio-preview-solution {
      width: 100%;
      font-size: 0.9em;
      h2 {
        margin: 12px 0 32px 0;
      }
      h4 {
        margin-bottom: 12px;
      }
      ul {
        margin-left: 16px;
      }
    }
  }

  .other-ongoing-projects {
    margin-top: 44px;
  }
  .what-people-say {
    display: flex;
    flex-wrap: wrap;
    h2 {
      width: 100%;
    }
    .what-people-say-testimonial {
      /* width: 50%; */
      padding-right: 16px;
      font-size: 0.9em;
      background: ${testimonialBg};
      padding: 92px 32px 32px 32px;
      margin-bottom: 32px;
      position: relative;
      margin-top: 60px;
      color: ${textColor};
      border: double 4px transparent;
      border-radius: 4px;

      .author-photo img {
        border: 3px solid ${testimonialBg};
        border-radius: 100px;
        overflow: hidden;
        margin-left: 16px;
        margin-top: 8px;
        margin-bottom: 18px;
        position: absolute;
        top: -50px;
        left: 10px;
      }
      .what-people-say-author {
        color: ${primaryColor};
        font-weight: 600;
        margin-bottom: 0;
      }
    }
  }
  .additional-work-thumbnails {
    width: 100%;
    display: flex;
    flex-direction: row;
    margin-right: 24px;
    .additional-work-thumbnail {
      padding: 16px;
      margin-right: 24px;

      border-radius: 4px;
      height: 200px;
      width: 160px;
      h3 {
        color: #fff;
      }
    }
  }
  .so-are-we-moving-forward {
  }
`;

export const WorkDisplayImg = styled.div<{ src: string }>`
  background: url(${props => `${props.src}?q=100&min-w=1000&fit=fill`})
    no-repeat center center;
  background-size: cover;
`;
