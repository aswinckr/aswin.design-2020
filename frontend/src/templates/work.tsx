import React from 'react';
import { graphql } from 'gatsby';
import { SanityWorksQueryQuery } from '../../graphqlTypes';
import Layout from '../components/layout';
import BlockContent from '@sanity/block-content-to-react';
import { RouteComponentProps } from '@reach/router';
import styled from 'styled-components';
import { textColor, highlightColor } from '../components/colors';
import Fade from 'react-reveal/Fade';
import { Link } from '@reach/router';
import { device } from '../utils';
import Button from '../components/button';
import Process from '../components/process';

interface Props extends RouteComponentProps {
  data: SanityWorksQueryQuery;
}

export const query = graphql`
  query SanityWorksQuery($slug: String) {
    allSanityWork(
      filter: { slug: { current: { ne: $slug } } }
      limit: 8
      sort: { fields: _createdAt, order: ASC }
    ) {
      edges {
        next {
          slug {
            current
          }
          title
          themeColor {
            hex
          }
          featured
          mainImage {
            asset {
              fluid {
                src
              }
            }
          }
        }
      }
    }

    sanityWork(slug: { current: { eq: $slug } }) {
      themeColor {
        hex
      }
      mainImage {
        asset {
          url
        }
      }
      title
      _rawBody(resolveReferences: { maxDepth: 10 })
    }
  }
`;

const WorkLayoutStyles = styled.div`
  .recommended-next {
    padding: 120px 0;
  }
  .all-projects {
    margin-top: 50px;
    @media ${device.tablet} {
      margin-top: 0;
    }
  }
  img {
    max-width: 100%;
    max-height: 670px;
    margin: 80px auto;
    display: block;
  }
  h2 {
    margin-top: 48px;
  }

  ul {
    margin-left: 0;
    padding-left: 32px;
    li {
    }
  }
  p,
  li {
    strong {
      text-decoration-line: underline;
    }
  }
  .additional-work-thumbnails {
    width: 100%;
    display: flex;
    flex-direction: row;
    margin-right: 24px;
    margin-top: 44px;

    .additional-work-thumbnail {
      margin-right: 24px;

      border-radius: 4px;
      height: 200px;
      width: 33.33%;
      h3 {
        color: ${textColor};
        margin-top: 16px;
      }
    }
  }
`;

const CoverBannerWrapper = styled.div`
  width: 100%;
  height: 500px;
  margin-bottom: 40px;
`;

const CoverBanner = styled.div<{ src: string | null | undefined }>`
  width: 100%;
  height: 500px;
  background: url(${props => `${props.src}?q=100&min-w=1000&fit=fill`})
    no-repeat center center;

  background-size: cover;
`;

const serializers = {
  types: {
    'design process': (props: any) => {
      const processList = props.node.process;
      return <Process processList={processList} />;
    },
  },
};

export default (props: Props) => (
  <Layout params={props.location ? props.location.search : ''}>
    <WorkLayoutStyles>
      <Fade>
        <CoverBannerWrapper
          style={{
            background: props.data.sanityWork?.themeColor?.hex!,
          }}
        >
          <CoverBanner src={props.data.sanityWork?.mainImage?.asset?.url} />
        </CoverBannerWrapper>

        <h1>{props.data.sanityWork?.title}</h1>
        {props.data.sanityWork !== null && (
          <BlockContent
            blocks={props.data.sanityWork._rawBody!}
            projectId={process.env.SANITY_PROJECT_ID}
            dataset={process.env.SANITY_PROJECT_DATASET}
            imageOptions={{ height: 300, fit: 'max' }}
            serializers={serializers}
          />
        )}
      </Fade>
      <div className="recommended-next">
        <h2>More Work</h2>
        <div className="additional-work-thumbnails">
          {props.data.allSanityWork.edges.map((edge, index) => {
            if (edge.next !== null && index < 5 && edge.next.featured) {
              return (
                <div
                  className="additional-work-thumbnail"
                  style={{ background: edge.next.themeColor?.hex! }}
                  key={index}
                >
                  <Link
                    to={`${edge.next.slug?.current!}/${
                      props.location ? props.location.search : ''
                    }`}
                  >
                    <div
                      style={{
                        background: `url(${
                          edge.next.mainImage?.asset?.fluid!.src
                        })`,
                        backgroundSize: `cover`,
                        width: '100%',
                        height: '100%',
                      }}
                    ></div>
                    <h3>{edge.next.title}</h3>
                  </Link>
                </div>
              );
            }
          })}
        </div>
      </div>
      <div className="all-projects" style={{ marginBottom: 80 }}>
        <Link to={`portfolio/${props.location ? props.location.search : ''}`}>
          <Button> See All Projects</Button>
        </Link>
      </div>
    </WorkLayoutStyles>
  </Layout>
);
