import { FixedObject, FluidObject } from 'gatsby-image';
import { SanityImageFixed, SanityImageFluid } from '../../graphqlTypes';

export const fixedImageNotNull = (image: SanityImageFixed): FixedObject => {
  const imageWithoutNull = {
    src: image.src && image.src !== null ? image.src : '',
    height: image.height && image.height !== null ? image.height : 100,
    width: image.width && image.width !== null ? image.width : 100,
    base64: image.base64 && image.base64 !== null ? image.base64 : '',
    aspectRatio:
      image.aspectRatio && image.aspectRatio !== null ? image.aspectRatio : 1,
    srcSet: image.srcSet && image.srcSet !== null ? image.srcSet : '',
    srcWebp: image.srcWebp && image.srcWebp !== null ? image.srcWebp : '',
    srcSetWebp:
      image.srcSetWebp && image.srcSetWebp !== null ? image.srcSetWebp : '',
  };
  return imageWithoutNull;
};

export const fluidImageNotNull = (image: SanityImageFluid): FluidObject => {
  const imageWithoutNull = {
    ...image,
    src: image.src && image.src !== null ? image.src : '',
    base64: image.base64 && image.base64 !== null ? image.base64 : '',
    sizes: image.sizes && image.sizes !== null ? image.sizes : '',
    aspectRatio:
      image.aspectRatio && image.aspectRatio !== null ? image.aspectRatio : 100,
    srcSet: image.srcSet && image.srcSet !== null ? image.srcSet : '',
    srcWebp: image.srcWebp && image.srcWebp !== null ? image.srcWebp : '',
    srcSetWebp:
      image.srcSetWebp && image.srcSetWebp !== null ? image.srcSetWebp : '',
  };
  return imageWithoutNull;
};
