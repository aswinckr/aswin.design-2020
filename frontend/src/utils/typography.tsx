// tslint:disable: object-literal-sort-keys
import Typography from 'typography';
import { primaryColor } from '../components/colors';

const typography = new Typography({
  baseFontSize: '18px',
  baseLineHeight: 1.7,
  headerLineHeight: 1.5,
  bodyFontFamily: ['Barlow', 'sans-serif'],
  headerFontFamily: ['Fugaz One', 'cursive'],
  googleFonts: [
    { name: 'Barlow', styles: ['400', '600'] },
    { name: 'Fugaz One', styles: ['400', '600'] },
  ],
  headerWeight: 600,
  bodyWeight: 400,
  scaleRatio: 2,
  overrideStyles: () => ({
    h1: {
      fontSize: '3em',
    },
    h2: {
      fontWeight: 600,
      color: primaryColor,
    },
    a: {
      fontWeight: 600,
    },
    li: {
      listStyle: 'circle',
    },
    p: {
      fontSize: '1em',
    },
  }),
});

export default typography;
