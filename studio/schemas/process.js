export default {
  name: "design process",
  title: "Insert Design Process",
  type: "object",
  fields: [
    {
      name: "process",
      title: "Design Process",
      type: "array",
      of: [{ name: "step", title: "Step", type: "string" }],
    },
  ],
};
